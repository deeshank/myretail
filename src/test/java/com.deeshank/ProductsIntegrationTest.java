package com.deeshank;

import com.deeshank.dao.ProductMapper;
import com.deeshank.models.Product;
import com.deeshank.resources.Price;
import com.deeshank.resources.ProductRequest;
import com.deeshank.resources.ProductTransaction;
import com.deeshank.resources.ProductsResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * Created by deeshank13 on 8/23/16.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductsIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @Autowired
    public ProductMapper dao;

    Product prod;

    @Before
    public void setUp() {
        prod = new Product("100","Pepsi", new Price("$1.95","USD"),"http://google.com?q=pepsi","100","ColdDrink","Beverages");
        dao.save(prod);

    }

    @After
    public void tearDown() {
        dao.delete(prod);
    }

    @Test
    public void testGET() {
        String productId = prod.getProductId();
        ResponseEntity<ProductsResponse> resp = restTemplate.getForEntity("http://localhost:8888/products?productId="+ productId, ProductsResponse.class);
        ProductsResponse p_resp = resp.getBody();
        List<com.deeshank.resources.Product> p = p_resp.getItems();

        assertThat(resp.getStatusCode(), is(HttpStatus.OK));
        assertEquals(1L, (long)p_resp.getTotalResults());
        assertEquals("Pepsi", p.get(0).getName());
        assertEquals("100", p.get(0).getId());
        assertEquals("$1.95", p.get(0).getCurrentPrice().getValue());
        assertEquals("USD", p.get(0).getCurrentPrice().getCurrencyCode());
        assertEquals("Beverages", p.get(0).getCategory());

    }


    @Test
    public void testPOST() {
        final RestTemplate template = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        ProductRequest p = new ProductRequest();
        p.setId("1000");
        p.setName("CocaCola");
        final HttpEntity<ProductRequest> request = new HttpEntity<>(p, requestHeaders);
        final ResponseEntity<ProductTransaction> response = template.exchange("http://localhost:8888/products", HttpMethod.POST, request, ProductTransaction.class);
        final ProductTransaction foo = response.getBody();
        assertThat(foo, notNullValue());
        assertEquals("1000",foo.getId());
        assertEquals("200",foo.getStatus());
    }

    @Test
    public void testPUT() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);


        ProductRequest p = new ProductRequest();
        p.setName("Coke");
        p.setId(prod.getProductId());
        p.setCategory("Cold Drinks");
        p.setImageUrl("https://google.com?q=coke");
        p.setBarcode("200");
        p.setDescription("Cold Drinks");
        p.setCurrentPrice(new Price("$1.40","USD"));

        final HttpEntity<ProductRequest> request = new HttpEntity<>(p,requestHeaders);
        // TODO: Error needs to be fixed
        // restTemplate.exchange("http://localhost:8888/products?productId="+ prod.getProductId(),HttpMethod.PUT,request,Void.class);

        //Product savedProduct = dao.findByProductId(prod.getProductId());
        //assertEquals("Coke",savedProduct.getProductName());
    }



}
