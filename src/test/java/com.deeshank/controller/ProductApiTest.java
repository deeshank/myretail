package com.deeshank.controller;

import com.deeshank.managers.ProductManager;
import com.deeshank.resources.Product;
import com.deeshank.resources.ProductsResponse;
import org.apache.http.client.utils.URIBuilder;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * @author gmatsu
 */
@Transactional
public class ProductApiTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    protected MockMvc mvc;

    @Mock
    ProductManager manager;

    @InjectMocks
    ProductsApiController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFindProductById() throws Exception {

        ProductsResponse response = getStubData();


        when(manager.findProductById("1")).thenReturn(response);

        URIBuilder b = new URIBuilder("/products");
        b.addParameter("productId", "1");

        String URI = b.toString();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Success - expected HTTP status", 200, status);
        verify(manager, times(1)).findProductById("1");

    }


    @Test
    public void testFindProductByName() throws Exception {

        ProductsResponse response = getStubData();


        when(manager.findProductByName("test")).thenReturn(response);

        URIBuilder b = new URIBuilder("/products");
        b.addParameter("productName", "test");

        String URI = b.toString();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("Success - expected HTTP status", 200, status);
        verify(manager, times(1)).findProductByName("test");

    }


    private ProductsResponse getStubData() {
        ProductsResponse response = new ProductsResponse();
        Product p = new Product();
        p.setId("1");
        p.setName("Test");
        response.setItems(new ArrayList<>());
        response.setTotalResults(1L);
        return response;
    }


}