package com.deeshank.controller;

import com.deeshank.managers.ProductManager;
import com.deeshank.resources.ProductRequest;
import com.deeshank.resources.ProductTransaction;
import com.deeshank.resources.ProductsResponse;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-21T17:24:13.923Z")

@Controller
public class ProductsApiController {

    @Autowired
    public ProductManager manager;

    @ApiOperation(value = "", notes = "get products info", response = ProductsResponse.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "find products", response = ProductsResponse.class)})
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<ProductsResponse> findProducts(
            @ApiParam(value = "product name") @RequestParam(value = "productName", required = false) String productName,
            @ApiParam(value = "product id") @RequestParam(value = "productId", required = false) String productId) {


        ProductsResponse response = null;

        if (productId != null && !"".equals(productId) && productName != null && !"".equals(productName)) {
            response = manager.findProduct(productId, productName);
        }else if (productName != null && !"".equals(productName)) {
            response = manager.findProductByName(productName);
        } else if (productId != null && !"".equals(productId)) {
            response = manager.findProductById(productId);
        }

        return new ResponseEntity<ProductsResponse>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "", notes = "post products info", response = ProductTransaction.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product Transaction", response = ProductTransaction.class)})
    @RequestMapping(value = "/products",
            method = RequestMethod.POST)
    public ResponseEntity<ProductTransaction> createProductTransaction(
            @ApiParam(value = "product info", required = true) @RequestBody ProductRequest product) {

        ProductTransaction response = manager.insertProduct(product);

        return new ResponseEntity<ProductTransaction>(response, HttpStatus.OK);
    }


    @ApiOperation(value = "", notes = "update products info", response = ProductTransaction.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product Transaction", response = ProductTransaction.class)})
    @RequestMapping(value = "/products",
            method = RequestMethod.PUT)
    public ResponseEntity<ProductTransaction> updateProductInfo(
            @ApiParam(value = "product id", required = true) @RequestParam(value = "id", required = true) String id,
            @ApiParam(value = "product info", required = true) @RequestBody ProductRequest product) {

        ProductTransaction response = manager.updateProduct(id,product);

        return new ResponseEntity<ProductTransaction>(response, HttpStatus.OK);
    }

}
