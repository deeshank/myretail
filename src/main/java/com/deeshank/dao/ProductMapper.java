package com.deeshank.dao;

import com.deeshank.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by deeshank13 on 8/21/16.
 */
public interface ProductMapper extends MongoRepository<Product, String> {

    @Query(value = "{'productName': {$regex:'.*?0.*',$options:'i'}}")
    List<Product> findByProductName(String productName);

    Product findByProductId(String productId);

    @Query(value = "{'productId': {$in:['?0']}}")
    List<Product> findByProductIds(String productId);



}
