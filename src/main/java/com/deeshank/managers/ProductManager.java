package com.deeshank.managers;

import com.deeshank.dao.ProductMapper;
import com.deeshank.models.Product;
import com.deeshank.resources.ProductRequest;
import com.deeshank.resources.ProductTransaction;
import com.deeshank.resources.ProductsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by deeshank13 on 8/22/16.
 */

@Service
public class ProductManager {


    @Autowired
    ProductMapper dao;

    public ProductsResponse findProductById(String id) {
        Product p = dao.findByProductId(id);
        com.deeshank.resources.Product prod = convert(p);

        List<com.deeshank.resources.Product> items = new ArrayList<>();
        items.add(prod);

        ProductsResponse resp = new ProductsResponse();
        resp.setItems(items);
        resp.setTotalResults((long) items.size());
        return resp;
    }

    public ProductsResponse findProductByName(String name) {
        List<Product> p = dao.findByProductName(name);


        List<com.deeshank.resources.Product> items = p.stream().map(this::convert).collect(Collectors.toList());

        ProductsResponse resp = new ProductsResponse();
        resp.setItems(items);
        resp.setTotalResults((long) items.size());
        return resp;
    }

    public ProductTransaction insertProduct(ProductRequest product) {
        Product p = new Product();
        if (isEmpty(product.getName()))
            p.setProductName(product.getName());
        if (isEmpty(product.getBarcode()))
            p.setBarcode(product.getBarcode());
        if (isEmpty(product.getDescription()))
            p.setDescription(product.getDescription());
        if (isEmpty(product.getId()))
            p.setProductId(product.getId());
        if (isEmpty(product.getCategory()))
            p.setCategory(product.getCategory());
        if (product.getCurrentPrice()!=null)
            p.setCurrent_price(product.getCurrentPrice());
        dao.save(p);
        ProductTransaction resp = new ProductTransaction();
        resp.setId(p.getProductId());
        resp.setStatus("200");
        return resp;
    }

    public ProductTransaction updateProduct(String id, ProductRequest product) {
        Product p = dao.findByProductId(id);
        if (product.getDescription()!=null && !"".equals(product.getDescription()))
            p.setDescription(product.getDescription());
        if (product.getBarcode()!=null && !"".equals(product.getBarcode()))
            p.setBarcode(product.getBarcode());
        if (product.getImageUrl()!=null && !"".equals(product.getImageUrl()))
            p.setImageUrl(product.getImageUrl());
        if (product.getCurrentPrice()!=null && !"".equals(product.getCurrentPrice()))
            p.setCurrent_price(product.getCurrentPrice());
        if (product.getId()!=null && !"".equals(product.getId()))
            p.setProductId(product.getId());
        if (product.getName()!=null && !"".equals(product.getName()))
            p.setProductName(product.getName());
        if (product.getCategory()!=null && !"".equals(product.getCategory()))
            p.setCategory(product.getCategory());
        dao.save(p);
        ProductTransaction resp = new ProductTransaction();
        resp.setId(p.getProductId());
        resp.setStatus("200");
        return resp;
    }

    public ProductsResponse findProduct(String productId, String productName) {
        Product p1 = dao.findByProductId(productId);
        List<Product> p2 = dao.findByProductName(productName);
        if (p1!=null){
            p2.add(p1);
        }
        List<com.deeshank.resources.Product> items = p2.stream().map(this::convert).collect(Collectors.toList());

        ProductsResponse resp = new ProductsResponse();
        resp.setItems(items);
        resp.setTotalResults((long) items.size());
        return resp;
    }


    com.deeshank.resources.Product convert(Product p){
        com.deeshank.resources.Product prod = new com.deeshank.resources.Product();
        prod.setName(p.getProductName());
        prod.setId(p.getProductId());
        prod.setImageUrl(p.getImageUrl());
        prod.setBarcode(p.getBarcode());
        prod.setCurrentPrice(p.getCurrent_price());
        prod.setDescription(p.getDescription());
        prod.setCategory(p.getCategory());
        return prod;
    }

    boolean isEmpty(String name){
        if (name!=null && !"".equals(name))
            return true;
        return false;
    }

}
