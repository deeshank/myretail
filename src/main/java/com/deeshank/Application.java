package com.deeshank;

import com.deeshank.dao.ProductMapper;
import com.deeshank.models.Product;
import com.deeshank.resources.Price;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;

@SpringBootApplication
@EnableSwagger2
@EnableAutoConfiguration
@Configuration
@ComponentScan(basePackages = "com.deeshank")
public class Application implements CommandLineRunner {

	@Autowired
	public ProductMapper product_dao;

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Override
	public void run(String... arg0) throws Exception {
		if (arg0.length > 0 && arg0[0].equals("exitcode")) {
			throw new ExitException();
		}
		product_dao.deleteAll();

		product_dao.save(new Product("11223344","DietCoke", new Price("$3.50","USD"),"http://google.com/#q=diet+coke","11223344","Soda","Food"));
		product_dao.save(new Product("11223300","Pepsi", new Price("$2.50","USD"),"http://google.com/#q=pepsi","11223300","Soda","Food"));
		product_dao.save(new Product("11234403","Coke", new Price("$2.50","USD"),"http://google.com/#q=coke","11234403","Soda","Food"));
		product_dao.save(new Product("12432401","Redbull", new Price("$3.00","USD"),"http://google.com/#q=redbull","12432401","Soda","Food"));
		product_dao.save(new Product("14562312","Arizona", new Price("$1.50","USD"),"http://google.com/#q=arizona","14562312","Soda","Food"));
		product_dao.save(new Product("24324566","Milk", new Price("$3.75","USD"),"http://google.com/#q=milk","24324566","Dairy","Food"));
		product_dao.save(new Product("25674342","Eggs", new Price("$3.50","USD"),"http://google.com/#q=eggs","25674342","Dairy","Food"));
		product_dao.save(new Product("27684534","Organic Eggs", new Price("$5.00","USD"),"http://google.com/#q=organic+eggs","27684534","Dairy","Food"));
		product_dao.save(new Product("21345298","Cheese", new Price("$4.25","USD"),"http://google.com/#q=cheese","21345298","Dairy","Food"));
		product_dao.save(new Product("20863471","Butter", new Price("$6.00","USD"),"http://google.com/#q=butter","20863471","Dairy","Food"));

		product_dao.save(new Product("32349567","Cinnamon Crunch Cereals", new Price("$3.75","USD"),"http://google.com/#q=cinnamon+cereal","32349567","Cereal","Food"));
		product_dao.save(new Product("30076456","Quaker Oats", new Price("$4.08","USD"),"http://google.com/#q=quaker+oats","30076456","Cereal","Food"));
		product_dao.save(new Product("33540120","Kellogg's Corn Flakes", new Price("$3.85","USD"),"http://google.com/#q=kellogs","33540120","Cereal","Food"));
		product_dao.save(new Product("35060709","Heinz Tomato Ketchup", new Price("$2.61","USD"),"http://google.com/#q=heinz+ketchup","35060709","Ketchup","Food"));
		product_dao.save(new Product("36699561","Uncle Lee's Tea", new Price("$4.19","USD"),"http://google.com/#q=tea","36699561","Tea","Food"));
		product_dao.save(new Product("40567923","Krafts Macaroni and Cheese", new Price("$4.75","USD"),"http://google.com/#q=krafts+mac","40567923","Mac and Cheese","Food"));
		product_dao.save(new Product("48345789","Godiva Assorted Chocolates", new Price("$11.48","USD"),"http://google.com/#q=godiva","48345789","Chocolate","Food"));
		product_dao.save(new Product("43562945","Similac Advanced Infant Formula with Iron powder", new Price("$15.15","USD"),"http://google.com/#q=similac+infant","43562945","Infant Food","Food"));
		product_dao.save(new Product("48956712","M&M's Chocolate Candy Party Size", new Price("$8.57","USD"),"http://google.com/#q=mandm","48956712","Chocolate","Food"));

		product_dao.save(new Product("51567234","HP 11inch Stream Laptop", new Price("$179.50","USD"),"http://google.com/#q=hp+stream+laptop","51567234","Laptop","Electronics"));
		product_dao.save(new Product("56612345","Epson Workforce All-in-one Printer", new Price("$82.75","USD"),"http://google.com/#q=epson+printer","56612345","Printer","Electronics"));
		product_dao.save(new Product("57239456","Acer Mineral Grey 14inch Laptop", new Price("$197.00","USD"),"http://google.com/#q=acer_laptop","57239456","Laptop","Electronics"));
		product_dao.save(new Product("59041234","Apple iPhone 5s 16GB", new Price("$149.50","USD"),"http://google.com/#q=apple+iphone+5s","59041234","Mobile","Electronics"));
		product_dao.save(new Product("55912340","At&t Samsung Go Prime Smartphone,Refurbished", new Price("$79.99","USD"),"http://google.com/#q=att+samsung+gp+prime","55912340","Mobile","Electronics"));
		product_dao.save(new Product("67204859","Otterbox Samsung Galaxy S4 Case", new Price("$12.50","USD"),"http://google.com/#q=otter+samsung+galaxy+s4","67204859","Mobile Case","Electronics"));
		product_dao.save(new Product("61034056","Insten Mini Phone Tripod", new Price("$7.99","USD"),"http://google.com/#q=phone+tripod","61034056","Mobile Tripod","Electronics"));
		product_dao.save(new Product("66923402","Cricket Wireless Web $40", new Price("$40.00","USD"),"http://google.com/#q=cricket+wireless","66923402","Mobile Sim Card","Electronics"));
		product_dao.save(new Product("64459063","2600mH Portable Charger Mobile", new Price("$6.99","USD"),"http://google.com/#q=portable+charger","64459063","Mobile Portable Charger","Electronics"));
		product_dao.save(new Product("60203045","Apple Cube and Lightening Cable 3inch", new Price("$10.88","USD"),"http://google.com/#q=apple+cable","60203045","Mobile Charger","Electronics"));


		product_dao.save(new Product("78234945","Lara Croft:Tomb Rider Blu ray", new Price("$4.50","USD"),"http://google.com/#q=lara+croft+tomb_rider","78234945","Movie CD","Movies and Music"));
		product_dao.save(new Product("77348592","Rambo the fight Continues", new Price("$4.90","USD"),"http://google.com/#q=rambo+fight+continues","77348592","Movie CD","Movies and Music"));
		product_dao.save(new Product("70810334","Workaholics-3 DVD", new Price("$7.45","USD"),"http://google.com/#q=workaholics+3","70810334","Movie CD","Movies and Music"));
		product_dao.save(new Product("71745027","Titanic", new Price("$8.50","USD"),"http://google.com/#q=titanic","71745027","Movie CD","Movies and Music"));
		product_dao.save(new Product("78275943","Hunger Games: Part 2 DVD ", new Price("$9.50","USD"),"http://google.com/#q=hunger+games","78275943","Movie CD","Movies and Music"));
		product_dao.save(new Product("86681394","Game of Thrones:The Complete Fifth Season ,Bluray", new Price("$52.50","USD"),"http://google.com/#q=game+of+thrones+fifth+season+cover","86681394","Movie CD","Movies and Music"));
		product_dao.save(new Product("80390456","The War", new Price("$42.50","USD"),"http://google.com/#q=the+war","80390456","Movie CD","Movies and Music"));
		product_dao.save(new Product("82384596","Harry Potter and the Cursed Child", new Price("$17.47","USD"),"http://google.com/#q=harry+potter","82384596","Series","Movies and Music"));
		product_dao.save(new Product("84059623","Nicolas Spark:The Notebook", new Price("$4.83","USD"),"http://google.com/#q=the+notebook","84059623","Movie CD","Movies and Music"));
		product_dao.save(new Product("89945439","Hozier", new Price("$12.50","USD"),"http://google.com/#q=hozier","89945439","Music Collection","Movies and Music"));

		product_dao.save(new Product("87823405","Colgate Optic White Sparkling Mint Toothpaste", new Price("$4.95","USD"),"http://google.com/#q=toothpaste+colgate","87823405","Toothpaste","Health and Beauty"));
		product_dao.save(new Product("86790090","Covergirl & Olay Ageless Foundation", new Price("$44.10","USD"),"http://google.com/#q=covergirl+foundation","86790090","Foundation","Health and Beauty"));
		product_dao.save(new Product("83011238","Covergirl Outstain Lipstain", new Price("$46.65","USD"),"http://google.com/#q=covergirl+lipstain","83011238","Lipstick","Health and Beauty"));
		product_dao.save(new Product("99094221","Covergirl Queen Collection Lasting Matte", new Price("$42.55","USD"),"http://google.com/#q=covergirl+matte","99094221","Matte","Health and Beauty"));
		product_dao.save(new Product("91191023","Radio Flyer Classic Tricylce", new Price("$50.50","USD"),"http://google.com/#q=tricycle","91191023","Tricycle for kids","Toys"));
		product_dao.save(new Product("95962345","Nickelodean Paw Patrol Plush Toy", new Price("$9.99","USD"),"http://google.com/#q=paw+patrol+toy","95962345","Plush Toy","Toys"));
		product_dao.save(new Product("91349583","Star Wars MAster Yoda", new Price("$49.00","USD"),"http://google.com/#q=yoda+action+figure","91349583","Action Figure Toy","Toys"));
		product_dao.save(new Product("99204059","Marvel Titan Hero Series : Hulk", new Price("$11.85","USD"),"http://google.com/#q=hulk+action+figure","99204059","Action Figure Toy","Toys"));
		product_dao.save(new Product("96923495","Ozark Trail Back Packing Tents", new Price("$39.50","USD"),"http://google.com/#q=camping+tent","96923495","Camping Tent","Sports and Outdoor"));
		product_dao.save(new Product("98843449","Skywalker 10inch Trampoline", new Price("$212.45","USD"),"http://google.com/#q=trampoline","98843449"," Galvanized Steel Frame","Sports and Outdoor"));

	}

	public static void main(String[] args) throws Exception {
		//new SpringApplication(Application.class).run(args);
        SpringApplication.run(Application.class, args);

	}

	class ExitException extends RuntimeException implements ExitCodeGenerator {
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode() {
			return 10;
		}

	}
}