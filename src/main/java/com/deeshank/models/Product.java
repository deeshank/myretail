package com.deeshank.models;

import com.deeshank.resources.Price;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by deeshank13 on 8/21/16.
 */

@Document(collection = "products")
@Data
public class Product {

    @Id
    private String id;

    @Indexed
    private String productName;

    @Indexed
    private String productId;

    private String imageUrl;

    private String barcode;

    private String description;

    private Price current_price;

    private String category;

    public String getCategory() {
        return category;
    }

    public Product(String productId,String productName, Price current_price, String imageUrl, String barcode, String description,  String category) {
        //this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.imageUrl = imageUrl;
        this.barcode = barcode;
        this.description = description;
        this.current_price = current_price;
        this.category = category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Product(String productId, String productName, Price price) {
        this.productName = productName;
        this.productId = productId;
        this.current_price = price;
    }

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Price getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(Price current_price) {
        this.current_price = current_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
